// Enum para el componente del estado del formulario (Si es para actualizar o subir)
export enum FormState {
  UPLOAD,
  UPDATE
}

// Enum para el componente loading
export enum LoadingState {
  LOADING = 'loading',
  ERROR = 'error',
  LOADED = 'loaded'
}
