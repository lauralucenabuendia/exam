import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

// Componentes
import {MovieCardComponent} from '../shared/Components/movie-card/movie-card.component';
import { LoadingComponent } from './Components/loading/loading.component';



@NgModule({
  declarations: [MovieCardComponent, LoadingComponent],
  imports: [CommonModule, IonicModule, FormsModule, MatIconModule],
  exports: [CommonModule, IonicModule, FormsModule, MatIconModule, MovieCardComponent, LoadingComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SharedModule { }
