import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FormState } from '../Models/enums';
import { Movie, Actors, Companies } from '../Models/interfaces';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  // Variable en la que dependiendo del valos que tenga
  // nos actualizará el formulario 'movie-form' para poder subir o actualizar películas
  private formMode : FormState;

  constructor(private http: HttpClient) { }

  setformMode(newState: FormState) {
    this.formMode = newState;
  }

  getformMode() {
    return this.formMode;
  }


  // -------------------------
  // CRUD
  // -------------------------
  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${environment.apiURL}/movies`)
  }

  getActors(): Observable<Actors[]> {
    return this.http.get<Actors[]>(`${environment.apiURL}/actors`)
  }

  getCompanies(): Observable<Companies[]> {
    return this.http.get<Companies[]>(`${environment.apiURL}/companies`)
  }

  uploadMovie(movie: Movie): Observable<Movie> {
    return this.http.post<Movie>(`${environment.apiURL}/movies`, movie);
  }

  updateMovie(movie: Movie): Observable<Movie> {
    return this.http.put<Movie>(`${environment.apiURL}/movies/${movie.id}`, movie);
  }

  deleteMovie(movieID: number): Observable<Movie> {
    return this.http.delete<Movie>(`${environment.apiURL}/movies/${movieID}`);
  }
}
