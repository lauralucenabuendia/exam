import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // {
  //   path: 'movie-details/:id',
  //   loadChildren: () => import('./Pages/movie-details/movie-details.module').then( m => m.MovieDetailsPageModule)
  // },
  {
    path: 'movie-form',
    loadChildren: () => import('./Pages/movie-form/movie-form.module').then( m => m.MovieFormPageModule)
  },
  {
    path: 'movie-form/:id',
    loadChildren: () => import('./Pages/movie-form/movie-form.module').then( m => m.MovieFormPageModule)
  },
  {
    path: 'movie-list',
    loadChildren: () => import('./Pages/movie-list/movie-list.module').then( m => m.MovieListPageModule)
  },
  {
    path: '',
    redirectTo: 'movie-list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
