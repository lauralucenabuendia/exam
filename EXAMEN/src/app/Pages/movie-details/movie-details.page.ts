import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { FormState, LoadingState } from 'src/app/shared/Models/enums';
import { Movie } from 'src/app/shared/Models/interfaces';
import { MovieService } from 'src/app/shared/Services/movie.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.page.html',
  styleUrls: ['./movie-details.page.scss'],
})
export class MovieDetailsPage implements OnInit {
  state: LoadingState = LoadingState.LOADING;
  movieID: number;
  movie: Movie;
  movieCompany: string;
  actors: string[] = [];
  numResponse: number = 0;
  numError: number = 0;


  //---------------------------------------
  // Ciclo de vida
  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private movieService: MovieService,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController) { }

  ngOnInit() {
    let actorsID: number[];
    this.movieID = Number(this.actRoute.snapshot.paramMap.get('id'));

    this.movieService.getMovies()
    .subscribe( (movies) => {
      this.movie = movies.find(m => m.id === this.movieID);
      actorsID = this.movie.actors;
      this.checkResponse(true);
    },
    error => {
      this.checkResponse(false);
    });

    this.movieService.getActors()
    .subscribe( (actors) =>{
      actorsID.forEach((id) => {
        const act = actors.find(a => a.id === id);
        if(act !== undefined && act.first_name !== undefined){
          this.actors.push(`${act.first_name} ${act.last_name}`);
        }
      });
      this.checkResponse(true);
    }, error => {
      this.checkResponse(false);
    });

    this.movieService.getCompanies().subscribe( (companies) =>{
      const companyFound = companies.find(c => {
        const find = c.movies.find(id => this.movieID === id);
        if(find){
          return c;
        }
      });
      if(companyFound !== undefined) {
        this.movieCompany = companyFound.name;
      }

      this.checkResponse(true);
    }, error => {
      this.checkResponse(false);
    });
  }

  // Comprobamos las respuestas de las peticiones y cambiar luego el estado de la página
  checkResponse(isSuccess: Boolean){
      if(isSuccess){
        this.numResponse++;
      } else {
        this.numError++;
      }

      if(this.numError + this.numResponse == 3){
          if(this.numError > 0){
            this.state = LoadingState.ERROR;
          } else {
            this.state = LoadingState.LOADED;
          }
      }
  }

  // Llama a una alerta para confirmar el borrado de la película
  deleteMovie(){
    this.deteleAlert();
  }

  // Nos lleva al formulario con la opción de actualizar una película
  updateMovie(){
    this.movieService.setformMode(FormState.UPDATE);
    this.router.navigateByUrl(`movie-form/${this.movieID}`);
  }

  // Alerta para confirmarnos si deseamos borrar o no la película
  async deteleAlert() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'INFORMATION',
      message: 'Are you sure that you want to delete this movie?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.state = LoadingState.LOADING;
            this.movieService.deleteMovie(this.movieID).subscribe( (resp)=>{
              this.state = LoadingState.LOADED;
              this.router.navigateByUrl('/movie-list');
              this.deleteMovieToast('Película borrada correctamente');
            },
            error => {
              this.state = LoadingState.LOADED;
              this.deleteMovieToast('No pudo borrarse la palícula');
            });
          }
        }
      ]
    });
    await alert.present();
  }

  // Toast que nos confirma el borrado de una película
  async deleteMovieToast(msg: string){
    const toast = await this.toastCtrl.create({
      message: msg,
      animated : true,
      duration: 2000,
      cssClass: 'toast-custom-class',
      position: 'bottom',
    });
    toast.present();
  }
}
