import { NgModule } from '@angular/core';
import { MovieDetailsPageRoutingModule } from './movie-details-routing.module';
import { MovieDetailsPage } from './movie-details.page';
import {SharedModule} from '../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [SharedModule,
    MovieDetailsPageRoutingModule,
    TranslateModule.forChild()],
  declarations: [MovieDetailsPage]
})
export class MovieDetailsPageModule {}
