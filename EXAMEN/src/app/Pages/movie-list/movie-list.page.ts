import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormState, LoadingState } from 'src/app/shared/Models/enums';
import { Movie } from 'src/app/shared/Models/interfaces';
import { MovieService } from 'src/app/shared/Services/movie.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.page.html',
  styleUrls: ['./movie-list.page.scss'],
})
export class MovieListPage{
  movieList: Movie[];
  state: LoadingState = LoadingState.LOADING;

  constructor(private movieService: MovieService,
    private router: Router) { }

  ionViewWillEnter(){
    this.movieService.getMovies().subscribe( (movies) => {
      this.movieList = movies;
      this.state = LoadingState.LOADED;
    },
    (error) => {
      this.state = LoadingState.ERROR;
    });
  }

  gotoDetails(id: number){
    this.movieService.setformMode(FormState.UPDATE);
    this.router.navigateByUrl(`/movie-list/${id}`);
  }
}
