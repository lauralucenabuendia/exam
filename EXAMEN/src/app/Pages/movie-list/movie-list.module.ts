import { NgModule } from '@angular/core';
import { MovieListPageRoutingModule } from './movie-list-routing.module';
import { MovieListPage } from './movie-list.page';
import {SharedModule} from '../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [SharedModule,
    MovieListPageRoutingModule,
    TranslateModule.forChild()],
  declarations: [MovieListPage]
})
export class MovieListPageModule {}
