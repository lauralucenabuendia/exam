import { NgModule } from '@angular/core';
import { MovieFormPageRoutingModule } from './movie-form-routing.module';
import { MovieFormPage } from './movie-form.page';
import {SharedModule} from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [SharedModule,
    MovieFormPageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild()],
  declarations: [MovieFormPage]
})
export class MovieFormPageModule {}
