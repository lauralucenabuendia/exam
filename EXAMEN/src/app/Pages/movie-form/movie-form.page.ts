import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { FormState, LoadingState } from 'src/app/shared/Models/enums';
import { Movie } from 'src/app/shared/Models/interfaces';
import { MovieService } from 'src/app/shared/Services/movie.service';

@Component({
  selector: 'app-movie-form',
  templateUrl: './movie-form.page.html',
  styleUrls: ['./movie-form.page.scss'],
})
export class MovieFormPage implements OnInit {
  // pelicual actual
  movie: Movie;

  movieform: FormGroup;
  actorsName: string[] = [];
  companies: string[] = [];
  state: LoadingState;

  // Para recoger de la base de datos los géneros de las películas sin que estos se repitan
  genres: Set<string>;

  // variables para mostrar cuando ya se han seleccionado
  genresSelected: string[];
  actorsSelected: string[];
  companySelected: string;
  actors: string[] = [];


  //-------------------------------------
  // Ciclo de vida
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private actRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private movieService: MovieService) {
      this.movieform = this.formBuilder.group({
        title:  ['', Validators.required],
        poster: ['', Validators.required],
        genre: ['', Validators.required],
        actors: ['', Validators.required],
        company: ['', Validators.required],
        year: ['', Validators.required],
        duration: ['', Validators.required],
        imdbRating: ['', Validators.required]
      });
    }

  ngOnInit() {
    const movieID = Number(this.actRoute.snapshot.paramMap.get('id'));

    if(this.movieService.getformMode() === FormState.UPDATE) {
      this.state = LoadingState.LOADING;
      this.movieService.getMovies()
      .subscribe( (movies) => {
        this.movie = movies.find(m => m.id === movieID);
        this.createForm();
        this.state = LoadingState.LOADED;
      },
      error => {
        this.state = LoadingState.ERROR;
      });
    } else {
      this.state = LoadingState.LOADED;
    }
  }


  //-------------------------------------
  // Métodos

  createForm(){
    if(this.movie !== undefined){
      this.movieform.controls.title.setValue(this.movie.title);
      this.movieform.controls.poster.setValue(this.movie.poster);
      this.movieform.controls.genre.setValue(this.movie.genre);
      this.movieform.controls.year.setValue(this.movie.year);
      this.movieform.controls.duration.setValue(this.movie.duration);
      this.movieform.controls.imdbRating.setValue(this.movie.imdbRating);

      this.movieService.getActors().subscribe( (actors) =>{
      this.movie.actors.forEach((id) => {
        const act = actors.find(a => a.id === id);
        if(act !== undefined && act.first_name !== undefined){
          this.actors.push(`${act.first_name} ${act.last_name}`);
        }
      });

      this.movieform.controls.actors.setValue(this.actors);
    });

    this.movieService.getCompanies().subscribe( (companies) =>{
      const companyFound = companies.find(c => {
        const find = c.movies.find(id => this.movie.id === id);
        if(find){
          return c;
        }
      });
      if(companyFound !== undefined) {
        this.movieform.controls.company.setValue(companyFound.name);
      }
    });
    }
    this.genres = new Set();

    // Obtenemos los géneros
    this.movieService.getMovies().subscribe((movies) =>{
      movies.forEach((movie) => {
        movie.genre.forEach(genre => this.genres.add(genre));
      });
    });

    // Obtenemos los actores
    this.movieService.getActors().subscribe((actors) => {
      actors.forEach( actor => {
        this.actorsName.push(`${actor.first_name} ${actor.last_name}`);
      });
    });

    // Obtenemos las compañías
    this.movieService.getCompanies().subscribe((companies) =>{
      companies.forEach((company => {
        this.companies.push(company.name);
      }));
    });
  }

  // Géneros
  onChangeGenres(genre: any){
    this.genresSelected = genre.detail as string[];
  }

  // Actores
  onChangeActors(actors: any){
    this.actorsSelected = actors.detail as string[];
  }

  // Estudio
  onChangeCompany(company: any){
    this.companySelected = company.detail as string;
  }

  // Subida del formmulario y controlar errores
  submitForm(){
    if(this.movieform.valid){
      const movie: Movie = this.movieform.value;
      movie.id = this.movie.id;

      this.state = LoadingState.LOADING;
      if(this.movieService.getformMode() === FormState.UPDATE){
        console.log(movie)
        this.movieService.updateMovie(movie)
        .subscribe((movie) =>{
          console.log(movie)
          this.state = LoadingState.LOADED;
          this.formAlert('movie edited successfully!');
        },
        (err)=>{
          this.state = LoadingState.LOADED;
          this.infoErrorToast(`error ${err}`);
        }
      );
      }else{
        this.movieService.uploadMovie(movie)
        .subscribe((movie) =>{
          this.state = LoadingState.LOADED;
          this.formAlert('movie created successfully!');
        },
        (err)=>{
          this.state = LoadingState.LOADED;
          this.infoErrorToast(`error ${err}`)
        });
      }
    }else{
      let errorMessage = `You forget to add this fields: `;

      if(this.movieform.controls.title.hasError('required')){
        errorMessage += `<br><b>-Title`
      }

      if(this.movieform.controls.poster.hasError('required')){
        errorMessage += `<br>-Poster`
      }

      if(this.movieform.controls.genres.hasError('required')){
        errorMessage += `<br>-Genres`
      }

      if(this.movieform.controls.actors.hasError('required')){
        errorMessage += `<br>-Actors`
      }

      if(this.movieform.controls.company.hasError('required')){
        errorMessage += `<br>-Company`
      }

      if(this.movieform.controls.year.hasError('required')){
        errorMessage += `<br>-Year`
      }

      if(this.movieform.controls.duration.hasError('required')){
        errorMessage += `<br>-Duration`
      }

      if(this.movieform.controls.puntuacion.hasError('required')){
        errorMessage += `<br>-Puntuacion</b>`
      }
      this.formAlert(errorMessage);
    }
  }

  // Nos informa del feedback acerca del formulario
  async formAlert(msg: string) {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'INFORMATION',
      message: msg,
      buttons: [{
        text: 'Yes',
      handler: () => {
        this.router.navigateByUrl('/movie-list');
      }
      }]
    });
    await alert.present();
  }

  async infoErrorToast(msg: string){
    const toast = await this.toastCtrl.create({
      message: msg,
      animated : true,
      duration: 2000,
      cssClass: 'toast-custom-class',
      position: 'bottom',
    });
    toast.present();
  }

}
