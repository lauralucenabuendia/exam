import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Peliculas', url: '/movie-list', icon: 'movie' },
    { title: 'Actores', url: '', icon: 'recent_actors' },
    { title: 'Estudios', url: '', icon: 'business' }
  ];

  constructor(public toastCtrl: ToastController,
    private translateService: TranslateService) {
      this.translateService.setDefaultLang('es');
    }

  // Toast para cuando intentamos ir a los otros enlaces
  async noPageCreatedToast(){
    const toast = await this.toastCtrl.create({
      message: 'Esta página aún no ha sido creada',
      animated : true,
      duration: 2000,
      cssClass: 'toast-custom-class',
      position: 'bottom',

    });
    toast.present();
  }
}
